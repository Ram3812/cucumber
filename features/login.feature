#Author: kuchi.sribhargavram2@mindtree.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Login
Feature: Test the Login Functionality

  Background: Follow till login page
    Given user on "https://atlanticbooks.com/" page
      And click on login
  
  @valid
  Scenario Outline: Test with valid credentials
     When user gives username <username> and password <password>
      And click on login button
     Then user navigates to home page
  
    Examples: 
      | username             | password | 
      | sbramkuchi@gmail.com | Password | 
  
  @invalid
  Scenario Outline: Test with invalid credentials
     When user gives username <username> and password <password>
      And click on login button
     Then error message displayed
  
    Examples: 
      | username       | password | 
      | test@gmail.com | 123456   | 
  
