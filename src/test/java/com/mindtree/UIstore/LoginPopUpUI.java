package com.mindtree.UIstore;

import org.openqa.selenium.By;

public class LoginPopUpUI {
	protected static By Email = By.id("email");
	protected static By Pswd = By.id("pass");
	protected static By SubmitBtn = By.id("send2");
	protected static By close = By.xpath("//div[@id='social-login-popup']/span");
	protected static By text = By.xpath("//div[@id='social-login-popup']/div[1]/div");
	protected static By msg = By.xpath("//*[@id=\"amsl-login-content\"]/p/div");
}
