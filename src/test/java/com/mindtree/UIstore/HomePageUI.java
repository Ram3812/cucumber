package com.mindtree.UIstore;

import org.openqa.selenium.By;

public class HomePageUI {
	protected static By LoginButton = By.xpath("//a[contains(text(),'Sign In')]");
	protected static By SearchBar = By.id("searchbox");
	protected static By SignOutBtn = By.xpath("//a[contains(text(),'Sign Out')]");

}
