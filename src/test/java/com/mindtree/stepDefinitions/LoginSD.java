package com.mindtree.stepDefinitions;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.mindtree.pageObject.HomePage;
import com.mindtree.pageObject.LoginPopUp;
import com.mindtree.utilities.Logging;
import com.mindtree.utilities.OpenBrowser;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
public class LoginSD extends OpenBrowser {
	public WebDriver Driver;
	private Logging loghelper = new Logging(this);
	private Logger logger = loghelper.CreateLogger();

	@Given("^user on \"([^\"]*)\" page$")
	public void user_on_something_page(String url) throws Throwable {
		System.out.println(url);
		Driver = initialiseDriver(prop);
		logger.info("Driver initialized...");
		Driver.get(url);
		Driver.manage().window().maximize();
	}

	@When("^user gives username (.+) and password (.+)$")
	public void user_gives_username_and_password(String username, String password) throws Throwable {
		System.out.println(username + ":  " + password);
		LoginPopUp LP = new LoginPopUp(Driver);
		LP.getEmail().sendKeys(username);
		LP.getPswd().sendKeys(password);
	}

	@Then("^user navigates to home page$")
	public void user_navigates_to_home_page() throws Throwable {
		System.out.println("nav to home");
		LoginPopUp LP = new LoginPopUp(Driver);
		Assert.assertEquals("You have successfully logged in.", LP.getLogInText());
		LP.getClose().click();
	}

	@Then("^error message displayed$")
	public void error_message_displayed() throws Throwable {
		System.out.println("err msg");
		LoginPopUp LP = new LoginPopUp(Driver);
		Assert.assertEquals(
				"The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.",
				LP.getErrText());
	}

	@And("^click on login$")
	public void click_on_login() throws Throwable {
		System.out.println("login");
		HomePage HP = new HomePage(Driver);
		HP.getLoginButton().click();
	}

	@And("^click on login button$")
	public void click_on_login_button() throws Throwable {
		System.out.println("login button");
		LoginPopUp LP = new LoginPopUp(Driver);
		LP.getSubmitBtn().click();
	}
}