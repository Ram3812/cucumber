package com.mindtree.runner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mindtree.pageObject.BooksPage;
import com.mindtree.pageObject.HomePage;
import com.mindtree.pageObject.LoginPopUp;
import com.mindtree.utilities.Logging;
import com.mindtree.utilities.OpenBrowser;
import com.mindtree.utilities.ReadExcelSheet;


public class Runner extends OpenBrowser {
	public WebDriver Driver;
	private Logging loghelper = new Logging(this);
	private Logger logger = loghelper.CreateLogger();

	@BeforeTest
	public void init() {
		Driver = initialiseDriver(prop);
	}

	@Test
	public void OpenUrl() {
		logger.info("Driver initialized...");
		String Url = prop.getProperty("url");
		Driver.get(Url);
		Driver.manage().window().maximize();
	}

	@Test(dependsOnMethods = { "OpenUrl" })
	public void Login() {
		HomePage HP = new HomePage(Driver);
		LoginPopUp LP = new LoginPopUp(Driver);
		HP.getLoginButton().click();
		LP.getEmail().sendKeys("sbramkuchi@gmail.com");
		LP.getPswd().sendKeys("Password");
		LP.getSubmitBtn().click();
	}

	@Test(dependsOnMethods = { "Login" })
	public void getBooks() {
		HomePage HP = new HomePage(Driver);
		HP.getSearchBar().sendKeys("GMAT");
		HP.getSearchBar().sendKeys(Keys.ENTER);
	}

	@Test(dependsOnMethods = { "getBooks" }, dataProvider = "getData")
	public void addToCart(String bookName) {
		BooksPage BP = new BooksPage(Driver);
		BP.getBook(bookName).click();
		BP.getAddToCartBTN().click();
		Driver.navigate().back();
	}
	
	@Test(dependsOnMethods = { "addToCart" })
	public void FailedTest() {
		Assert.assertEquals(Driver.getTitle().toString(), "Buy");
	}

	@AfterTest
	public void SignOut() {
		HomePage HP = new HomePage(Driver);
		HP.getSignOutBtn().click();
	}

	@DataProvider
	public Object[] getData() {
		return ReadExcelSheet.getBook(prop.getProperty("books"), "BookName");
	}

	@AfterTest(dependsOnMethods = { "SignOut" })
	public void closeBrowser() {
		Driver.close();
	}
}
