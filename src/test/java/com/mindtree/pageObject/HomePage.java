package com.mindtree.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.UIstore.HomePageUI;

public class HomePage extends HomePageUI {
	private WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getLoginButton() {
		return driver.findElement(LoginButton);
	}

	public WebElement getSearchBar() {
		return driver.findElement(SearchBar);
	}

	public WebElement getSignOutBtn() {
		return driver.findElement(SignOutBtn);
	}
}
