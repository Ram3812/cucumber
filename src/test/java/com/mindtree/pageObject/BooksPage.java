package com.mindtree.pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.UIstore.BooksPageUI;

public class BooksPage extends BooksPageUI {

	private WebDriver driver;

	public BooksPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getBook(String bookName) {
		String Selector = "//a[contains(text()," + "'" + bookName + "'" + ")]";
		return driver.findElement(By.xpath(Selector));
	}

	public WebElement getAddToCartBTN() {
		return driver.findElement(AddToCartBTN);
	}

}
