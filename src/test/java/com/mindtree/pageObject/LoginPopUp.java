package com.mindtree.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.UIstore.LoginPopUpUI;

public class LoginPopUp extends LoginPopUpUI {
	private WebDriver driver;

	public LoginPopUp(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEmail() {
		return driver.findElement(Email);
	}

	public WebElement getPswd() {
		return driver.findElement(Pswd);
	}

	public WebElement getSubmitBtn() {
		return driver.findElement(SubmitBtn);
	}
	
	public String getLogInText() {
		return driver.findElement(text).getText();
	}
	
	public String getErrText() {
		return driver.findElement(msg).getText();
	}
	
	public WebElement getClose() {
		return driver.findElement(close);
	}
}
